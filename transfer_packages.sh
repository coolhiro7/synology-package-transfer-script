#!/bin/ash

# Synology Package Transfer Script
# made by Hiro Choi(hiro.choi.dev@gmail.com)
# It is tested on DSM 7.2.
# If you want to use it, check synology directory structure first.
# It will be unavailable if directory structure is changed when DSM is updated.

# USAGE
# 1. Stop your all packages as possible as you can.
# 2. Change "src" and "target" environments below. Only volume name is available.
# 3. Run script with privileged permission.

# change src and target.
src="volume2"
target="volume1"

declare -a dir_synology_list=("@appconf" "@apphome" "@appstore" "@apptemp" "@appdata")
declare -a dir_list=("etc" "home" "target" "tmp" "var")

# move packages files.
for d in $(ls /${src}/@appstore);
do
    echo "d : $d"
    cd "/${src}/@appstore/$d"
    current_dir_name=$(basename "$PWD")
    for sdir in "${dir_synology_list[@]}"
    do
        echo "mv /${src}/${sdir}/${current_dir_name} /${target}/${sdir}/"
        mv "/${src}/${sdir}/${current_dir_name}" "/${target}/${sdir}/"
    done
done

# change package links.
for d in $(ls /${target}/@appstore);
do
    echo "cd /var/packages/$d"
    cd /var/packages/$d
    current_dir_name=$(basename "$PWD")
    echo "current_dir_name : $current_dir_name"
    for i in "${!dir_list[@]}"
    do
        echo "rm ${dir_list[$i]}"
        rm "${dir_list[$i]}"
        full_dir_name="/${target}/${dir_synology_list[$i]}/${current_dir_name}"
        echo "ln -s $full_dir_name ${dir_list[$i]}"
        ln -s "$full_dir_name" "${dir_list[$i]}"
    done
done
